#include "File.h"

int File::newID = 0;

File::File(const string& a_name) : name(a_name), id(newID) {
	newID++;
}

FileList::FileList() {
	head = NULL;
	tail = NULL;
}

File* FileList::add(const string& name) {
	if(tail) {
		tail->next = new Node(name);
		tail = tail->next;
	}
	else {
		head = tail = new Node(name);
	}
	return tail->file;
}

void FileList::erase() {
	if(head) {
		delete head;
		head = NULL;
	}
}

FileList::~FileList() {
	erase();
}

FileList::Node::Node(const string& name) {
	file = new File(name);
	next = NULL;
}

FileList::Node::~Node() {
	delete file;
	if(next) delete next;
}