CC=g++
CFLAGS=-g -Wall
LFLAGS=-g
EXECUTABLE=db
SOURCES=$(wildcard *.cc)
OBJS=$(SOURCES:.cc=.o)

include .depend

all: .depend $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJS)
	@echo "Budowanie $(EXECUTABLE)"
	$(CC) $(OBJS) $(LFLAGS) -o $(EXECUTABLE)

.cc.o:
	@echo "Budowanie $@"
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -f *.o
	
.depend: $(SRCS)
	rm -f ./.depend
	$(CC) $(CFLAGS) -MM $^>>./.depend;

