/* 
 *	Paweł Kubik
 *	Klasa przechowująca zaindeksowane słowo
 * 
 */
#include "Occurrence.h"
#include <string>

using namespace std;

#ifndef WORD_H
#define	WORD_H

class Word {
private:
	Occurrence occur;
public:
	const string text;
	Word(const string& a_text);
	void addOccurrence(const File& a_file);
	void addOccurrence(const Occurrence& occ);
	void manualAddOccurrence(const File& a_file, int count) { occur.manualAdd(a_file, count); };
	const Occurrence& occurances() const;
};

#endif	/* WORD_H */

