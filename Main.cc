/*
 *	Paweł Kubik
 *	Funkcja main()
 *
 */

#include "Database.h"

using namespace std;

int main() {
	Database db;
	Database::printInit();
	while(!db.exit) {
		db.getCommand();
	}
	return 0;
}
