/* 
 *	Paweł Kubik
 *	Drzewo binarne
 */

#include "Tree.h"

Tree::Tree() {
	root = NULL;
}

Tree::Node::Node(const string& text, Node* par) : word(text) {
	parent = par;
	factor = 0;
	left = NULL;
	right = NULL;
}

int Tree::Node::cmp(const string& str) const {
	return -word.text.compare(str);
}

int Tree::Node::simiCmp(const string& str) const {
	return simiCompare(word.text, str);
}

Tree::Node*& Tree::find(const string& str, Node** par) {
	Node** it = &root;
	Node* parent = NULL;
	while(*it) {
		int c = (*it)->cmp(str);
		if(c < 0) {
			parent = *it;
			it = &((*it)->left);
		}
		else if(c > 0) {
			parent = *it;
			it = &((*it)->right);
		}
		else {
			break;
		}
	}
	if(par)
		*par = parent;
	return *it;
}

Word& Tree::addNode(const string& str) {
	Node* node;
	Node*& ref = find(str, &node);
	Node* result = ref;
	if(!ref) {
		ref = new Node(str, node);
		result = ref;

		if(node && node->left == ref) {
			ref->side = -1;
		}
		else {
			ref->side = 1;
		}

		node = ref;
		Node* parent;

		while(node && node->parent) {
			node->parent->factor+=node->side;
			if(node->parent->factor == 0)
				break;
			else if(node->parent->factor >= 2) {
				parent = node->parent;
				if(node->factor <= -1) {
					if(node->left->factor == 0) {
						node->factor = 0;
						node->parent->factor = 0;
					}
					else if(node->left->factor > 0) {
						node->factor = 0;
						node->parent->factor = -1;
					}
					else {
						node->parent->factor = 0;
						node->factor = 1;
					}
					node->left->factor = 0;
					node->rotateRight(root);
				}
				else {
					node->factor = 0;
					node->parent->factor = 0;
				}
				parent->rotateLeft(root);

				break;
			}
			else if(node->parent->factor <= -2) {
				parent = node->parent;
				if(node->factor >= 1) {
					if(node->right->factor == 0) {
						node->factor = 0;
						node->parent->factor = 0;
					}
					else if(node->right->factor > 0) {
						node->factor = 0;
						node->parent->factor = 1;
					}
					else {
						node->parent->factor = 0;
						node->factor = -1;
					}
					node->rotateLeft(root);
				}
				else {
					node->factor = 0;
					node->parent->factor = 0;
				}
				parent->rotateRight(root);
				break;
			}

			node = node->parent;
		}
	}
	return result->word;
}

void Tree::add(const string& str, const File& a_file) {
	addNode(str).addOccurrence(a_file);
}

void Tree::add(const Word& w) {
	addNode(w.text).addOccurrence(w.occurances());
}

void Tree::Node::rotateLeft(Node*& root) {
	Node* tmp = right;
	right->parent = parent;
	right = right->left;
	if(right)
		right->parent = this;
	tmp->left = this;
	if(parent) {
		if(side < 0)
			parent->left = tmp;
		else
			parent->right = tmp;
	}
	else {
		root = tmp;
	}
	parent = tmp;
	tmp->side = side;
	if(right)
		right->side = 1;
	side = -1;
}

void Tree::Node::rotateRight(Node*& root) {
	Node* tmp = left;
	left->parent = parent;
	left = left->right;
	if(left)
		left->parent = this;
	tmp->right = this;
	if(parent) {
		if(side < 0)
			parent->left = tmp;
		else
			parent->right = tmp;
	}
	else {
		root = tmp;
	}
	parent = tmp;
	tmp->side = side;
	if(left)
		left->side = -1;
	side = 1;
}

Tree::Iterator Tree::findWord(const string& str) const {
	Node* it = root;
	while(it) {
		int c = it->cmp(str);
		if(c < 0) {
			it = it->left;
		}
		else if(c > 0) {
			it = it->right;
		}
		else {
			break;
		}
	}
	return Iterator(it);
}

Tree::Iterator Tree::findSimiWord(const string& str, Iterator start) const {
	if(start.isNULL()) {
		start = begin();
	}
	else {
		++start;
	}
	
	while(!start.isNULL() && start.simiCmp(str)) {
		++start;
	}
	return start;
}

Tree::Iterator Tree::begin() const {
	Node* n = root;
	if(n) {
		while(n->left) {
			n = n->left;
		}
	}
	return Iterator(n);
}

Tree::Iterator::Iterator() {
	node = NULL;
}

Tree::Iterator::Iterator(Node* n) {
	node = n;
}

void Tree::Iterator::operator ++() {
	if(node->right) {
		node = node->right;
		while(node->left) {
			node = node->left;
		}
	}
	else {
		while(node && node->side == 1) {
			node = node->parent;
		}
		if(node) {
			node = node->parent;
		}
	}
}

bool Tree::Iterator::isNULL() {
	return (node == NULL);
}

Word& Tree::Iterator::getWord() {
	return node->word;
}

void Tree::erase() {
	if(root) {
		Node* n = root;
		while(n->left || n->right) {
			if(n->left)
				n = n->left;
			else 
				n = n->right;
		}
		while(n->parent) {
			if(n->side == -1) {
				n = n->parent;
				delete n->left;
				if(n->right) {
					n = n->right;
					while(n->left || n->right) {
						if(n->left)
							n = n->left;
						else 
							n = n->right;
					}
				}
			}
			else {
				n = n->parent;
				delete n->right;
			}
		}
		delete n;
		root = NULL;
	}
}

Tree::~Tree() {
	erase();
}

int simiCompare(const string& s1, const string& s2) {
	int diff = 0;
	unsigned int maxlen, minlen;
	if(s1.length() < s2.length()) {
		maxlen = s2.length();
		minlen = s1.length();
	}
	else {
		maxlen = s1.length();
		minlen = s2.length();
	}
	for(unsigned int i=0;i<maxlen;i++) {
		if(i>=minlen || s1[i] != s2[i]) {
			diff++;
		}
	}
	if(diff == 1)
		diff--;
	return diff;
}