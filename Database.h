/* 
 *	Paweł Kubik
 *	Baza danych
 *
 */

#include "Tree.h"

using namespace std;

#ifndef DATABASE_H
#define	DATABASE_H

class Database {
private:
	Tree words;
	FileList files;
public:
	Database();
	void printWords();
	void printWordStats(const string& str) const;
	void printSimiWordsStats(const string& str) const;
	void erase() { words.erase(); files.erase(); }
	void addFile(const string& str);
	void saveToFile(const string& str);
	void loadFromFile(const string& str);
	void getCommand();
	void doCommand(const string& str);
	bool exit;
	static void printInit();
};

void print(const Word& word);

#endif	/* DATABASE_H */

