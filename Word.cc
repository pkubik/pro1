#include "Word.h"
#include <string>

using namespace std;

Word::Word(const string& a_text) : text(a_text) {
	
}

const Occurrence& Word::occurances() const {
	return occur;
}

void Word::addOccurrence(const File& a_file) {
	occur.add(a_file);
}

void Word::addOccurrence(const Occurrence& occ) {
	occur = occur+occ;
}