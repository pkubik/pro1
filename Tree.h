/* 
 *	Paweł Kubik
 *	Drzewo binarne
 */

#include <string>
#include "Word.h"

#ifndef TREE_H
#define	TREE_H

class Tree {
private:
	class Node;
	
	Node* root;
	Node*& find(const string& str, Node** par=NULL);
	Word& addNode(const string& str);
public:
	class Iterator; //in-order
	
	Tree();
	Tree(const Tree& orig);	
	void add(const string& str ,const File& a_file);
	void add(const Word& w);
	Iterator findWord(const string& str) const;
	Iterator findSimiWord(const string& str, Iterator start) const;
	Iterator begin() const; //in-order
	void erase();
	~Tree();
};

class Tree::Node {
public:
	Node* left;
	Node* right;
	Node* parent;
	int factor;
	int side;	//-1 jesli jest lewym potomkiem, +1 prawym
	Word word;
	Node(const string& text, Node* par);
	int cmp(const string& str) const;
	int simiCmp(const string& str) const;
	void rotateLeft(Node*& root);
	void rotateRight(Node*& root);
};

class Tree::Iterator {
private:
	Node* node;
public:
	Iterator();
	Iterator(Node* n);
	void operator ++();
	bool isNULL();
	int cmp(const string& str) const { return node->cmp(str); };
	int simiCmp(const string& str) const { return node->simiCmp(str); };
	Word& getWord();
};

int simiCompare(const string& s1, const string& s2);

#endif	/* TREE_H */

