/* 
 *	Paweł Kubik
 *	Klasa wystapien slowa w poszczegolnych plikach
 *
 */

#include "Occurrence.h"
#include <algorithm>

Occurrence::Occurrence() {
	head = NULL;
	tail = NULL;
	current = NULL;
	references = new int(1);
}

Occurrence::Occurrence(const Occurrence& occ) {
	if(&occ != this) {
		head = occ.head;
		current = occ.current;
		tail = occ.tail;
		references = occ.references;
		(*references)++;
	}
}

Occurrence::Node::Node(const File& a_file, Node* a_prev) {
	file = &a_file;
	count = 1;
	prev = a_prev;
	next = NULL;
}

void Occurrence::add(const File& a_file) {
	if(current && current->file->id == a_file.id) {
		current->count++;
		if(current->prev && (current->count > current->prev->count) ) {
			swap(current->count, current->prev->count);	//przesuniecie w gore posortowanej listy
			swap(current->file,current->prev->file);	//szybciej kopiuje sie pola niz wskazniki
			current = current->prev;
		}
	}
	else if(tail) {
		tail->next = new Node(a_file, tail);
		tail = tail->next;
		current = tail;
	}
	else {
		head = new Node(a_file, NULL);
		tail = head;
		current = tail;
	}
}

void Occurrence::manualAdd(const File& a_file, int count) {
	if(tail) {
		tail->next = new Node(a_file, tail);
		tail = tail->next;
		tail->count = count;
		current = tail;
	}
	else {
		head = new Node(a_file, NULL);
		tail = head;
		tail->count = count;
		current = tail;
	}
}

Occurrence& Occurrence::operator =(const Occurrence& occ) {
	if(&occ != this) {
		destruct();
		head = occ.head;
		current = occ.current;
		tail = occ.tail;
		references = occ.references;
		(*references)++;
	}
	return *this;
}

Occurrence Occurrence::operator +(const Occurrence& occ) const {
	Occurrence result;
	Iterator i1(head);
	Iterator i2(occ.head);
	while( !(i1.isNULL() && i2.isNULL()) ) {
		if(i1.getCount() >= i2.getCount()) {
			result.add(i1.getFile());
			result.tail->count = i1.getCount();
			++i1;
		}
		else {
			result.add(i2.getFile());
			result.tail->count = i2.getCount();
			++i2;
		}
	}

	return result;
}

Occurrence::Iterator::Iterator() {
	node = NULL;
}

Occurrence::Iterator::Iterator(Node* a_node) {
	node = a_node;
}

void Occurrence::Iterator::operator ++() {
	node = node->next;
}

const File& Occurrence::Iterator::getFile() {
	return *(node->file);
}

int Occurrence::Iterator::getCount() {
	if(node)
		return node->count;
	else
		return 0;	//ulatwienie w algorytmach
}

bool Occurrence::Iterator::isNULL() {
	return (node == NULL);
}

Occurrence::Iterator Occurrence::begin() const {
	return Iterator(head);
}

void Occurrence::destruct() {
	if((*references) <= 1) {
		if(head) {
			Node* it = head;
			while(it->next != NULL) {
				it = it->next;
				delete it->prev;
			}
			delete it;
		}
		delete references;
	}
	else {
		(*references)--;
	}
}

Occurrence::~Occurrence() {
	destruct();
}