/* 
 *	Paweł Kubik
 *	Baza danych
 *
 */

#include "Database.h"
#include <iostream>
#include <fstream>

using namespace std;

void print(const Word& word) {
	cout<<word.text<<"\n";
	for(Occurrence::Iterator it=word.occurances().begin();!it.isNULL();++it) {
		cout<<it.getFile().name<<" - "<<it.getCount()<<" occurance(s)\n";
	}
}

Database::Database() {
	exit = false;
}

void Database::printWords() {
	bool empty = true;
	for(Tree::Iterator it=words.begin();!it.isNULL();++it) {
		cout<<it.getWord().text<<" ";
		empty = false;
	}
	if(empty) {
		cout<<"The database is empty.";
	}
	cout<<"\n";
}

void Database::addFile(const string& str) {
	if(str.compare(".") && str.compare("..")) {
		File* f = files.add(str);
		string word;
		ifstream fs;
		fs.open(str.c_str());
		if(fs.fail()) {
			cout<<"Failed to read file \""<<str<<"\".\n";
		}
		else {
			while(true) {
				fs>>word;
				if(fs.fail()) break;
				words.add(word, *f);
			} 
		}
		fs.close();
	}
	else {
		cout<<"Failed to read file \""<<str<<"\".\n";
	}
}

void Database::printWordStats(const string& str) const {
	Tree::Iterator it = words.findWord(str);
	if(it.isNULL()) {
		cout<<"Word \""<<str<<"\" not found.\n";
	}
	else {
		print(it.getWord());
	}
}

void Database::printSimiWordsStats(const string& str) const {
	bool found = false;
	Tree::Iterator it;
	it = words.findSimiWord(str, it);
	while(!it.isNULL()) {
		found = true;
		print(it.getWord());
		it = words.findSimiWord(str, it);
	}
	if(!found) {
		cout<<"No word similar to \""<<str<<"\" found.\n";
	}
}

void Database::saveToFile(const string& str) {
	ofstream fs;
	fs.open(str.c_str());
	if(fs.is_open()) {
		for(Tree::Iterator it=words.begin();!it.isNULL();++it) {
			fs<<it.getWord().text<<"\n";
			for(Occurrence::Iterator i=it.getWord().occurances().begin();!i.isNULL();++i) {
				fs<<">"<<i.getFile().name<<" "<<i.getCount()<<"\n";
			}
		}
	}
}

void Database::loadFromFile(const string& str) {
	string text;
	int num;
	Word* word = NULL;
	ifstream fs;
	fs.open(str.c_str());
	if(!fs) {
		cout<<"Failed to read file \""<<str<<"\".\n";
	}
	else {
		while(true) {
			fs>>text;
			if(fs.fail()) break;
			if(text[0] == '>') {
				fs>>num;
				word->manualAddOccurrence(*files.add(text.substr(1)), num);
			}
			else {
				if(word) {
					words.add(*word);
					delete word;
				}
				word = new Word(text);
			}
			
		} 
		
		if(word) {
			words.add(*word);
			delete word;
		}
	}
	fs.close();
}

void Database::getCommand() {
	string cmd;
	cin>>cmd;
	if(cmd[0] == ':') {
		doCommand(cmd.substr(1));
	}
	else {
		addFile(cmd);
	}
}

void Database::doCommand(const string& str) {
	if(!str.compare("list")) {
		printWords();
	}
	else if(!str.compare("find")) {
		string arg;
		cin>>arg;
		printWordStats(arg);
	}
	else if(!str.compare("find~")) {
		string arg;
		cin>>arg;
		printSimiWordsStats(arg);
	}
	else if(!str.compare("save")) {
		string arg;
		cin>>arg;
		saveToFile(arg);
	}
	else if(!str.compare("load")) {
		string arg;
		cin>>arg;
		loadFromFile(arg);
	}
	else if(!str.compare("reload")) {
		string arg;
		cin>>arg;
		erase();
		loadFromFile(arg);
	}
	else if(!str.compare("erase")) {
		erase();
	}
	else if(!str.compare("quit")) {
		exit = true;
	}
	else {
		cout<<"Command not recognised.\n";
	}
}

void Database::printInit() {
	cout<<"Database of words in indexed files.\n";
	cout<<"Any entered word will result in adding a file of corresponding name\n";
	cout<<"unless it's preceded by ':' sign.\nThose are interpreted as following commands:\n";
	cout<<" :load [filename]\t- loads saved DB from [filename]\n";
	cout<<" :save [filename]\t- saves database to [filename]\n";
	cout<<" :find [word]\t\t- prints occurrences of [word]\n";
	cout<<" :find~ [word]\t\t- prints occurrences of words similar to [word]\n";
	cout<<" :erase\t\t\t- erases database\n";
	cout<<" :list\t\t\t- prints all words in the database\n";
	cout<<" :quit\t\t\t- quit\n";
}