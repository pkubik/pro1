/* 
 *	Paweł Kubik
 *	Klasa reprezentujaca plik
 *
 */
#include <string>

using namespace std;

#ifndef FILE_H
#define	FILE_H

class File {
private:
	static int newID;
public:
	const string name;
	const int id;
	File(const string& a_name);
};

class FileList {
private:
	class Node {
	public:
		File* file;
		Node* next;
		Node(const string& name);
		~Node();
	};
	Node* head;
	Node* tail;
public:
	FileList();
	File* add(const string& name);
	void erase();
	~FileList();
};

#endif	/* FILE_H */

