/* 
 *	Paweł Kubik
 *	Klasa wystąpień słowa w poszczególnych plikach
 *
 */

#include "File.h"

using namespace std;

#ifndef OCCURANCE_H
#define	OCCURANCE_H

class Occurrence {
private:
	class Node;
	
	Node* head;
	Node* tail;
	Node* current;
	int* references;
	void destruct();
public:
	class Iterator;
	
	Occurrence();
	Occurrence(const Occurrence& occ);
	void add(const File& a_file);
	void manualAdd(const File& a_file, int count);
	Occurrence& operator =(const Occurrence& occ);
	Occurrence operator +(const Occurrence& occ) const;	//dodaje listy zachowując sortowanie
	Iterator begin() const;
	~Occurrence();
};

class Occurrence::Node {
public:
	Node* next;
	Node* prev;
	int count;
	const File* file;

	Node(const File& a_file, Node* a_prev);
};

class Occurrence::Iterator {
private:
	Node* node;
public:
	Iterator();
	Iterator(Node* a_node);
	void operator ++();
	const File& getFile();
	int getCount();
	bool isNULL();
};

#endif	/* OCCURANCE_H */

